# Maintainer: Bernhard Landauer <oberon@manjaro.org>

pkgname=plasma-mobile-settings
pkgver=20220812
pkgrel=2
arch=('any')
url="https://gitlab.manjaro.org/manjaro-arm/packages/community/plasma-mobile/$pkgname"
license=('GPL')
pkgdesc='Settings files for Plasma mobile'
depends=('accountsservice' 'noto-fonts' 'breeze' 'breeze-gtk' 'systemd')
makedepends=('git')
source=('applications-blacklistrc'
    'disable-random-mac.conf'
    'kdeglobals'
    'kscreenlockerrc'
    'kwinrc'
    'mimeapps.list'
    'kxkbrc'
    'startkderc'
    'sddm.conf'
    'packagekit-offline.sh'
    '91_plasma-mobile.gschema.override'
    'drkonqi-coredump-launcher.desktop'
    'plasmamobile.json'
    'settings.ini'
    'powerdevil.hook'
    '20-pinephone-led.rules')
sha256sums=('db89a855aa5e6070fcfbe659f40497e048ec8a7e8624ee02e9a0bb967eaaf488'
            '490bd6bda16408da9bd9ac331f5dc344a4874f04f2b257a0a1083b969f7c857a'
            '62e8771e2e068c3e6e9ca5dfc055bf73a141766611565d525236980fc0f4a415'
            '569f5280e4650b8490751b5bb41e373b370999afa46440d89b0d878a4280e3eb'
            '85e5198e665282d14241eef9c80068dc23bc7c32afd54af202c65d9d081bc9ea'
            'd5fc3b3d88d9cdbe15bf724ece4382cd4ae95b1f7ebfe7c748f8d89a120c4b7f'
            '9527bacca5286a929bde7a89e1fd1a19aa1f0f67f227874d01120f02c17be30b'
            'bf14819bb36a95cbb465169ca3640d8a93f9a648881f9f52f6315a1ba40d273c'
            '97200b73d2ff5c989f0b94d9d1189a81b4e54a6746420b389067a62934c148ad'
            'e879ca7b637199e9ab5edd04c1b0c0da6b775ee925c21435f14fad426abe7e62'
            'fd5a89ce81197a912a9879aa5aa33daf68c8f80a4b70192fff89364ef9d18734'
            '21e9614f3d09bd9b0f5912a2d79c77fd37092326abde36073d9a79624f101d6a'
            '42db6e099ab56aa4f6c01107fe83897a059bc371a389379cd3c260d4ab8fd2bc'
            'e3e671c730f7a4ed54011a1152a54e5ce11e6092850c379deb5641d88b5463bd'
            '42c4de4c9310904ada1c84e23a34e42fcf20f8318a5f798372468381965dbd18'
            'ec6f1eae0c8bf6829fe84237b46ab770bbb6ec0fecb0d385aa70590fd466b900')

pkgver() {
    date +%Y%m%d
}

package() {
    xdg=$pkgdir/etc/xdg
    install -Dm644 applications-blacklistrc $xdg/applications-blacklistrc
    install -Dm644 disable-random-mac.conf $pkgdir/etc/NetworkManager/conf.d/disable-random-mac.conf
    install -Dm644 kdeglobals $xdg/kdeglobals
    install -Dm644 kscreenlockerrc $xdg/kscreenlockerrc
    install -Dm644 kwinrc $xdg/kwinrc
    install -Dm644 startkderc $xdg/startkderc
    install -Dm644 sddm.conf $pkgdir/etc/sddm.conf.d/00-default.conf
    install -Dm755 packagekit-offline.sh $pkgdir/etc/profile.d/packagekit-offline.sh
    install -Dm644 "${srcdir}/91_plasma-mobile.gschema.override" -t "${pkgdir}/usr/share/glib-2.0/schemas"
    install -Dm644 "${srcdir}/plasmamobile.json" -t "${pkgdir}/usr/share/maliit/keyboard2/devices/"
    install -Dm644 "${srcdir}/settings.ini" -t "${pkgdir}/etc/skel/.config/gtk-3.0/"
    install -Dm644 20-pinephone-led.rules -t "${pkgdir}/usr/lib/udev/rules.d/"
    install -Dm644 drkonqi-coredump-launcher.desktop -t "${pkgdir}/etc/skel/.config/autostart/"
    install -Dm644 mimeapps.list -t "${xdg}/"
    install -Dm644 kxkbrc -t "${xdg}/"
    # install alpm hook
    install -Dm644 "$srcdir/powerdevil.hook" "$pkgdir/usr/share/libalpm/hooks/90-powerdevil.hook"    
}
